/*Author: Rakibul Hasan Reday
 * Student id:19224765
 * This code to show the data need to send to rasspberry pi which will show 3 Axis of sensosr , gps data, time stamp and error code
 */
#include "Wire.h"
#include "I2Cdev.h"
#include "MPU6050.h"
#define USE_GPS 1
#include "LoRaWan.h"

#ifdef USE_GPS

#include <TinyGPS++.h>
#include <SoftwareSerial.h>

TinyGPSPlus gps;

#endif

 
MPU6050 accelgyro;
I2Cdev   I2C_M;

uint8_t buffer_m[6];


int16_t ax, ay, az;
int16_t gx, gy, gz;
int16_t   mx, my, mz;



float heading;
float tiltheading;

float Axyz[3];
float Gxyz[3];
float Mxyz[3];


#define sample_num_mdate  5000      

volatile float mx_sample[3];
volatile float my_sample[3];
volatile float mz_sample[3];

static float mx_centre = 0;
static float my_centre = 0;
static float mz_centre = 0;

volatile int mx_max =0;
volatile int my_max =0;
volatile int mz_max =0;

volatile int mx_min =0;
volatile int my_min =0;
volatile int mz_min =0;

void setupGyro() {
  // join I2C bus (I2Cdev library doesn't do this automatically)
  Wire.begin();

  // initialize serial communication
  // (38400 chosen because it works as well at 8MHz as it does at 16MHz, but
  Serial.begin(38400);
  pinMode(38, OUTPUT);
  digitalWrite(38, HIGH);
  while(!Serial);
  // initialize device
  //Serial.println("Initializing I2C devices...");
  accelgyro.initialize();

  // verify connection
  //Serial.println("Testing device connections...");
  //Serial.println(accelgyro.testConnection() ? "MPU9250 connection successful" : "MPU9250 connection failed");
  
  delay(3000);//set delay for 3 second
  //Serial.println("     ");
 
  Mxyz_init_calibrated ();
  
}


 

void readGps()

{
char c;

#ifdef USE_GPS

    bool locked;

#endif

 

    Serial.begin(115200);

    while(!Serial);

 

    lora.init();

    lora.setDeviceReset();

 

#ifdef USE_GPS

    Serial2.begin(9600);     // open the GPS

    locked = false;

 

    // For S&G, let's get the GPS fix now, before we start running arbitary

    // delays for the LoRa section

 

    while (!gps.location.isValid()) {

      while (Serial2.available() > 0) {

        if (gps.encode(c=Serial2.read())) {

          //displayInfo();

          if (gps.location.isValid()) {

           locked = true;

            break;

          }

        }

//        SerialUSB.print(c);

      }

 

   if (locked)
       break;

 

      if (millis() > 15000 && gps.charsProcessed() < 10)

      {

        SerialUSB.println(F("No GPS detected: check wiring."));

        SerialUSB.println(gps.charsProcessed());

        while(true);

      } 

      else if (millis() > 20000) {

        SerialUSB.println(F("Not able to get a fix in alloted time."));     

        break;

      }

    }

#endif
 

}

 void setup()
{
  Serial2.begin(9600);
  setupGyro();//set up gyro
  readGps();// read gps
  //added these extra line to see data properly on serial monitor
  Serial.println("Latitude        Longitude            Date         Time         Axis1    Axis2   Axis3");
  Serial.println("                                                                                     ");
  Serial.println("------------------------------------------------------------------------------------ ");

}

 

void loop(void)

{
getAccel_Data();
displayInfo();
//Serial.print(",Axis1: "+String(Axyz[0])+", Axis2: "+String(Axyz[1])+", Axis3: "+String(Axyz[2])+"}");
  
delay(3000);

}

 

void displayInfo()

{

  //SerialUSB.print(F("")); 

  if (gps.location.isValid())

  {

    SerialUSB.print(gps.location.lat(), 6);

    SerialUSB.print(F("     "));

    SerialUSB.print(gps.location.lng(), 6);
  }

  else

  {

    SerialUSB.print(F("**"));

  }

 

  SerialUSB.print(F("        "));

  if (gps.date.isValid())

  {

    SerialUSB.print(gps.date.day());

    SerialUSB.print(F("/"));

    SerialUSB.print(gps.date.month());

    SerialUSB.print(F("/"));

    SerialUSB.print(gps.date.year());
  }

  else

  {

    SerialUSB.print(F("**"));

  }

 

  SerialUSB.print(F("      "));

  if (gps.time.isValid())

  {

    if (gps.time.hour() < 10) SerialUSB.print(F("0"));

    SerialUSB.print(gps.time.hour());

    SerialUSB.print(F(":"));

    if (gps.time.minute() < 10) SerialUSB.print(F("0"));

    SerialUSB.print(gps.time.minute());

    SerialUSB.print(F(":"));

    if (gps.time.second() < 10) SerialUSB.print(F("0"));

    SerialUSB.print(gps.time.second());

    SerialUSB.print(F("."));

    if (gps.time.centisecond() < 10) SerialUSB.print(F("0"));

    SerialUSB.print(gps.time.centisecond());

  }

  else

  {

    SerialUSB.print(F("**"));

  }

 Serial.print("     "+String(Axyz[0])+"    "+String(Axyz[1])+"   "+String(Axyz[2]));

  SerialUSB.println();

}

void getHeading(void)
{
  heading=180*atan2(Mxyz[1],Mxyz[0])/PI;
  if(heading <0) heading +=360;
}

void getTiltHeading(void)
{
  float pitch = asin(-Axyz[0]);
  float roll = asin(Axyz[1]/cos(pitch));

  float xh = Mxyz[0] * cos(pitch) + Mxyz[2] * sin(pitch);
  float yh = Mxyz[0] * sin(roll) * sin(pitch) + Mxyz[1] * cos(roll) - Mxyz[2] * sin(roll) * cos(pitch);
  float zh = -Mxyz[0] * cos(roll) * sin(pitch) + Mxyz[1] * sin(roll) + Mxyz[2] * cos(roll) * cos(pitch);
  tiltheading = 180 * atan2(yh, xh)/PI;
  if(yh<0)    tiltheading +=360;
}



void Mxyz_init_calibrated ()
{
  
  Serial.println(F("Before using 9DOF,we need to calibrate the compass frist,It will takes about 2 minutes."));
  Serial.print("  ");
  Serial.println(F("During  calibratting ,you should rotate and turn the 9DOF all the time within 2 minutes."));
  Serial.print("  ");
  Serial.println(F("If you are ready ,please sent a command data 'ready' to start sample and calibrate."));
  while(!Serial.find("ready")); 
  Serial.println("  ");
  Serial.println("ready");
  Serial.println("Sample starting......");
  Serial.println("waiting ......");
  
  get_calibration_Data ();
  
  Serial.println("     ");
  Serial.println("compass calibration parameter ");
  Serial.print(mx_centre);
  Serial.print("     ");
  Serial.print(my_centre);
  Serial.print("     ");
  Serial.println(mz_centre);
  Serial.println("    ");
}


void get_calibration_Data ()
{
    for (int i=0; i<sample_num_mdate;i++)
      {
      get_one_sample_date_mxyz();
      /*
      Serial.print(mx_sample[2]);
      Serial.print(" ");
      Serial.print(my_sample[2]);                            //you can see the sample data here .
      Serial.print(" ");
      Serial.println(mz_sample[2]);
      */


      
      if (mx_sample[2]>=mx_sample[1])mx_sample[1] = mx_sample[2];     
      if (my_sample[2]>=my_sample[1])my_sample[1] = my_sample[2]; //find max value      
      if (mz_sample[2]>=mz_sample[1])mz_sample[1] = mz_sample[2];   
      
      if (mx_sample[2]<=mx_sample[0])mx_sample[0] = mx_sample[2];
      if (my_sample[2]<=my_sample[0])my_sample[0] = my_sample[2];//find min value
      if (mz_sample[2]<=mz_sample[0])mz_sample[0] = mz_sample[2];
            
      }
      
      mx_max = mx_sample[1];
      my_max = my_sample[1];
      mz_max = mz_sample[1];      
          
      mx_min = mx_sample[0];
      my_min = my_sample[0];
      mz_min = mz_sample[0];
  

  
      mx_centre = (mx_max + mx_min)/2;
      my_centre = (my_max + my_min)/2;
      mz_centre = (mz_max + mz_min)/2;  
  
}






void get_one_sample_date_mxyz()
{   
    getCompass_Data();
    mx_sample[2] = Mxyz[0];
    my_sample[2] = Mxyz[1];
    mz_sample[2] = Mxyz[2];
} 


void getAccel_Data(void)
{
  accelgyro.getMotion9(&ax, &ay, &az, &gx, &gy, &gz, &mx, &my, &mz);
  Axyz[0] = (double) ax / 16384;//16384  LSB/g
  Axyz[1] = (double) ay / 16384;
  Axyz[2] = (double) az / 16384; 
}

void getGyro_Data(void)
{
  accelgyro.getMotion9(&ax, &ay, &az, &gx, &gy, &gz, &mx, &my, &mz);
  Gxyz[0] = (double) gx * 250 / 32768;//131 LSB(��/s)
  Gxyz[1] = (double) gy * 250 / 32768;
  Gxyz[2] = (double) gz * 250 / 32768;
}

void getCompass_Data(void)
{
  I2C_M.writeByte(MPU9150_RA_MAG_ADDRESS, 0x0A, 0x01); //enable the magnetometer
  delay(10);
  I2C_M.readBytes(MPU9150_RA_MAG_ADDRESS, MPU9150_RA_MAG_XOUT_L, 6, buffer_m);
  
    mx = ((int16_t)(buffer_m[1]) << 8) | buffer_m[0] ;
  my = ((int16_t)(buffer_m[3]) << 8) | buffer_m[2] ;
  mz = ((int16_t)(buffer_m[5]) << 8) | buffer_m[4] ;  
  
  //Mxyz[0] = (double) mx * 1200 / 4096;
  //Mxyz[1] = (double) my * 1200 / 4096;
  //Mxyz[2] = (double) mz * 1200 / 4096;
  Mxyz[0] = (double) mx * 4800 / 8192;
  Mxyz[1] = (double) my * 4800 / 8192;
  Mxyz[2] = (double) mz * 4800 / 8192;
}

void getCompassDate_calibrated ()
{
  getCompass_Data();
  Mxyz[0] = Mxyz[0] - mx_centre;
  Mxyz[1] = Mxyz[1] - my_centre;
  Mxyz[2] = Mxyz[2] - mz_centre;  
}
